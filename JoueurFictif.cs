﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Puissance4
{
    public class JoueurFictif
    {
        private Game gameLocal;

        public JoueurFictif(Game g)
        {
            gameLocal = g;
        }

        public int joueBetement()
        {
            Random rnd = new Random();
            return gameLocal.NewToken(rnd.Next(0, 6));
        }
        public int joueIntelligemment()
        {
            var color = gameLocal._currentPlayerColor == TokenColor.Red ? TokenColor.Yellow : TokenColor.Red;
            return ouJouer(color);
        }

        public int ouJouer(TokenColor tokenColor)
        {
            int DG, DD, HH, VV, y;
            for (int i = 0; i < 7; i++)
            {
                y = calculLigneAJouer(i);
                if (y == -1)
                    continue;
                else
                {
                    DG = gameLocal.LineCount(i, y, +1, +1, tokenColor) + gameLocal.LineCount(i, y, -1, -1, tokenColor);
                    if (DG == 4)
                        return i;

                    DD = gameLocal.LineCount(i, y, -1, +1, tokenColor) + gameLocal.LineCount(i, y, +1, -1, tokenColor);
                    if (DD == 4)
                        return i;

                    HH = gameLocal.LineCount(i, y, +1, 0, tokenColor) + gameLocal.LineCount(i, y, -1, 0, tokenColor);
                    if (HH == 4)
                        return i;

                    VV = gameLocal.LineCount(i, y, 0, +1, tokenColor);
                    if (VV == 3)
                        return i;
                }
            }
            Random rnd = new Random();
            return gameLocal.NewToken(rnd.Next(0, 6));
        }
        public int calculLigneAJouer(int column)
        {
            for (int i = 5; i >= 0; i--)
                if (gameLocal._tokens[column, i] == TokenColor.None)
                {
                    return i;
                }
            return -1;
        }


    }
}
