﻿using System;

namespace Puissance4
{
    public class Game
    {
        public TokenColor[,] _tokens = new TokenColor[7, 6];
        public TokenColor _currentPlayerColor = TokenColor.Yellow;

        public TokenColor CurrentPlayerColor
        {
            get
            {
                if (_currentPlayerColor == TokenColor.Yellow)
                    _currentPlayerColor = TokenColor.Red;
                return _currentPlayerColor;
            }
        }

        public event EventHandler<WonGameEventArgs> WonGame;

        public TokenColor GetToken(int x, int y)
        {
            return _tokens[x, y];
        }

        public int NewToken(int column)
        {
            for (int i = 5; i >= 0; i--)
                if (_tokens[column, i] == TokenColor.None)
                {
                    _tokens[column, i] = _currentPlayerColor;
                    if (IsThisAWin(column, i, _currentPlayerColor))
                    {
                        OnWonGame(column, i, _currentPlayerColor);
                    }
                    return i;
                }
            return -1;
        }

        public virtual void OnWonGame(int x,int y, TokenColor color)
        {
            WonGame?.Invoke(this, new WonGameEventArgs(x,y,color));
        }

        public bool IsThisAWin(int x, int y, TokenColor tokenColor)
        {
            var DG = LineCount(x, y, +1, +1, tokenColor) +
                LineCount(x, y, -1, -1, tokenColor);

            if (DG == 5)
               return true;

            var DD = LineCount(x, y, -1, +1, tokenColor) +
                LineCount(x, y, +1, -1, tokenColor);
            if (DD == 5)
                return true;

            var H = LineCount(x, y, +1, 0, tokenColor) +
                LineCount(x, y, -1, 0, tokenColor);
            if (H == 5)
                return true;

            var V = LineCount(x, y, 0, +1, tokenColor);
            if (V == 4) 
                return true;

            return false;
        }
        public int LineCount(int x, int y, int vx, int vy, TokenColor tokenColor)
        {
            int result = 0;
            if (x > 6 || x < 0) return result;
            if (y > 5 || y < 0) return result;
            result = 1;
            if (_tokens[x, y] == tokenColor) return result + LineCount(x + vx, y + vy, vx, vy, tokenColor);

            return 0;
        }


    }
}
