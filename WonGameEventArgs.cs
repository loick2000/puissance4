﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Puissance4
{
    public class WonGameEventArgs : EventArgs
    {
        public WonGameEventArgs(int x, int y, TokenColor color)
        {
            X = x;
            Y = y;
            Color = color;
        }

        public int X { get; }
        public int Y { get; }
        public TokenColor Color { get; }
    }
}
