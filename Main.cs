﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Puissance4
{
    public partial class Main : Form
    {
        private int _previousWidth;
        private int _previousHeight;
        private const int cstPadding = 5;
        private bool jeuSeul = false;
        private bool IA = false;
        private Rectangle[,] _circleZones = new Rectangle[7, 6];

        private Game game;
        private JoueurFictif joueurIA;

        public Main()
        {
            InitializeComponent();
            game = new Game();
            joueurIA = new JoueurFictif(game);
            game.WonGame += Game_WonGame;
        }

        private void Game_WonGame(object sender, WonGameEventArgs e)
        {
            dessineToken(e.X, e.Y, e.Color);
            MessageBox.Show($"Bravo, {game._currentPlayerColor} a gagné!");
            NouvellePartie();
        }

        private void Main_Paint(object sender, PaintEventArgs e)
        {
            using (var formGraphics = CreateGraphics())
            using (var blueBrush = new SolidBrush(Color.DarkKhaki))
            using (var whiteBrush = new SolidBrush(Color.White))
            using (var yellowBrush = new SolidBrush(Color.Yellow))
            using (var redBrush = new SolidBrush(Color.Red))
            {
                var circleWidth = ClientSize.Width / 7;
                var circleHeight = (ClientSize.Height - menuStrip1.Height - lblCurrentPlayer.Height) / 6;

                formGraphics.FillRectangle(blueBrush, 0, 0, ClientSize.Width, ClientSize.Height);

                for (var i = 0; i < 7; i++)
                    for (var j = 0; j < 6; j++)
                    {
                        var x = i * circleWidth;
                        var y = j * circleHeight + menuStrip1.Height + lblCurrentPlayer.Height;

                        var zone = new Rectangle(i * circleWidth + cstPadding, j * circleHeight + menuStrip1.Height + lblCurrentPlayer.Height,
                            circleWidth - cstPadding * 2, circleHeight - cstPadding * 2);
                        _circleZones[i, j] = zone;

                        var brush = whiteBrush;

                        if (game._tokens[i, j] == TokenColor.Yellow)
                        {
                            brush = yellowBrush;
                        }
                        else if (game._tokens[i, j] == TokenColor.Red)
                        {
                            brush = redBrush;
                        }

                        formGraphics.FillEllipse(brush, zone);
                    }
            }
        }

        private void Main_Resize(object sender, EventArgs e)
        {
            if (ClientSize.Width < _previousWidth || ClientSize.Height < _previousHeight)
            {
                Refresh();
            }
            _previousWidth = ClientSize.Width;
            _previousHeight = ClientSize.Height;
        }

        private void Main_Load(object sender, EventArgs e)
        {
            _previousWidth = ClientSize.Width;
            _previousHeight = ClientSize.Height;

            ChangeCurrentPlayer();
        }

        private void Main_MouseClick(object sender, MouseEventArgs e)
        {
            for (var i = 0; i < 7; i++)
            {
                if (_circleZones[i, 0].Location.X <= e.X && e.X <= _circleZones[i, 0].Location.X + _circleZones[i, 0].Width)
                {
                    var yFreeToken = game.NewToken(i);

                    if (yFreeToken == -1) return;

                    dessineToken(i, yFreeToken, game._currentPlayerColor);
                    ChangeCurrentPlayer();
                    if (!jeuSeul)
                    {
                        IA = !IA;
                        if (IA)
                        {
                            var IAnewToken = joueurIA.joueIntelligemment();
                            if (IAnewToken == -1) return;
                            dessineToken(i, IAnewToken, game._currentPlayerColor);
                            IA = !IA;
                            ChangeCurrentPlayer();
                        }
                    }
                    break;
                }
            }
        }
        private void dessineToken(int x, int y, TokenColor color)
        {
            var zone = _circleZones[x, y];
            using (var formGraphics = CreateGraphics())
            using (var brush = new SolidBrush(game._currentPlayerColor == TokenColor.Yellow ? Color.Yellow : Color.Red))
            {
                formGraphics.FillEllipse(brush, zone);
            }

        }
        private void nouvellePartieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NouvellePartie();
            jeuSeul = true;
        }
        private void recommencerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NouvellePartie();
            jeuSeul = false;
        }

        private void NouvellePartie()
        {
            game._currentPlayerColor = TokenColor.Yellow;
            ChangeCurrentPlayer();
            for (var i = 0; i < 7; i++)
                for (var j = 0; j < 6; j++)
                {
                    game._tokens[i, j] = TokenColor.None;
                }
            Refresh();
        }


        public void ChangeCurrentPlayer()
        {
            game._currentPlayerColor = game._currentPlayerColor == TokenColor.Yellow ? TokenColor.Red : TokenColor.Yellow;
            lblCurrentPlayer.Text = "Current Player : " + game._currentPlayerColor.ToString();
        }

    }

    public enum TokenColor
    {
        None,
        Yellow,
        Red
    }
}
